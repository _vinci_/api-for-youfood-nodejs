const express = require('express');
const { response } = require('express');


const routes = require('./routes');

const app  = express();

app.use(express.json())
app.use(express.urlencoded())

app.use(routes)


app.listen(3333)