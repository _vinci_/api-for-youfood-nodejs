const express = require('express')
const connection = require('./database/connection')  

const routes = express.Router();

routes.post('/dishes', async (request, response) => {
    
    const {id, nome, descrição, detalhe, preço, imagem1, imagem2} = request.body;


    await connection('dishes').insert({
        id,
        nome,
        descrição,
        detalhe,
        preço,
        imagem1,
        imagem2

    })
    
    return response.json ({
       nome
    })
})


routes.get('/dishes', async (request, response) => {
    
    const dishes = await connection('dishes').select('*')


   
    
    return response.json (
     dishes)
})



module.exports = routes;