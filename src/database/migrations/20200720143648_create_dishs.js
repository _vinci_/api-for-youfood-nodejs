
exports.up = function(knex) {
  return knex.schema.createTable('dishes', function(table){
      table.increments('id').notNullable()
      table.string('nome').notNullable()
      table.string('descrição').notNullable()
      table.string('detalhe').notNullable()
      table.integer('preço').notNullable()
      table.string('imagem1')
      table.string('imagem2')
    })
};

exports.down = function(knex) {
  return knex.schema.dropTable('dishes')
};
